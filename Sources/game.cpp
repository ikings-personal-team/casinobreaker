//
//  game.cpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "game.hpp"
#include <iostream>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <map>
#include "constants.hpp"

namespace game {
	
	const std::map<Mode, std::string> modeName = {
		{ lcg, "Lcg" },
		{ mt, "Mt" },
		{ betterMt, "BetterMt" }
	};
	
	MakeBetResponse makeBet(Mode mode, Account& account, int64_t bet, int64_t number) {
		const std::string url = constants::PlayUrl + modeName.at(mode) +
			"?id=" + account.id +
			"&bet=" + std::to_string(bet) +
			"&number=" + std::to_string(number);
		
		curlpp::Cleanup cleanup;
		
		std::ostringstream os;
		os << curlpp::options::Url(url);
		
		json_t json = json_t::parse(os.str());
		
		if (json.find("error") != json.end()) {
			std::string error = json.at("error").get<std::string>();
			throw std::runtime_error(error);
		}
		
		account.money = json.at("account").get<Account>().money;
		return json;
	}
	
	void to_json(json_t& json, const MakeBetResponse& r) {
		json = json_t{{"message", r.message}, {"realNumber", r.realNumber}};
	}
	
	void from_json(const json_t& json, MakeBetResponse& r) {
		r.message = json.at("message").get<std::string>();
		r.realNumber = json.at("realNumber").get<int64_t>();
	}

}

std::ostream& operator<<(std::ostream& ostream, const game::MakeBetResponse& response) {
	ostream << response.message << std::endl << "Real number: " << response.realNumber;
	return ostream;
}
