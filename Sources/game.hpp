//
//  game.hpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#ifndef game_hpp
#define game_hpp

#include <stdio.h>
#include "account.hpp"

namespace game {
	
	enum Mode {
		lcg,
		mt,
		betterMt
	};
	
	struct MakeBetResponse {
		std::string message;
		int64_t realNumber;
	};
	
	MakeBetResponse makeBet(Mode mode, Account& account, int64_t bet, int64_t number);
	
	void playLcg(Account& account, int64_t goal = 1000000);
	void playMt(Account& account, int64_t goal = 1000000);
	void playBetterMt(Account& account, int64_t goal = 1000000);
	
	void to_json(json_t& json, const MakeBetResponse& r);
	void from_json(const json_t& json, MakeBetResponse& r);
	
}

std::ostream& operator<<(std::ostream& ostream, const game::MakeBetResponse& response);

#endif /* game_hpp */
