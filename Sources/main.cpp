//
//  main.cpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include <iostream>
#include "game.hpp"

const std::string accountId = "<account_id>";

int main(int argc, const char * argv[]) {
	
	Account account;
	try {
		account = Account::create(accountId);
	} catch (const std::exception& exeption) {
		std::cout << "Error occured during account creation: " << exeption.what() << std::endl;
		return 1;
	}
	
	std::cout << account << std::endl;
	
	try {
		game::playMt(account);
	} catch (const std::exception& exeption) {
		std::cout << "Error occured during playing: " << exeption.what() << std::endl;
		return 1;
	}
	
	return 0;
}
