//
//  helpers.hpp
//  CasinoBreaker
//
//  Created by iKing on 16.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#ifndef helpers_hpp
#define helpers_hpp

#include <stdio.h>
#include <tuple>

namespace helpers {
	
	struct EuclidResult {
		int64_t gcd;
		int64_t x;
		int64_t y;
	};
	
	EuclidResult extended_euclid(int64_t a, int64_t b);
}

#endif /* helpers_hpp */
