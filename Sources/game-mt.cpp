//
//  game-mt.cpp
//  CasinoBreaker
//
//  Created by iKing on 16.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "game.hpp"
#include <vector>
#include <iostream>
#include <random>

namespace game {
	
	void playMt(Account& account, int64_t goal) {
		MakeBetResponse response = makeBet(mt, account, 1, 0);
		std::cout << response << std::endl;
		std::cout << "Current money: " << account.money << std::endl;
		int32_t realNumber = (int32_t) response.realNumber;
		
		std::mt19937 generator;
		bool seedDiscovered = false;
		std::time_t time = std::time(nullptr);
		int32_t seed = (int32_t) time;
		for (int32_t offset = 0; offset <= 1000; ++offset) {
			generator.seed(seed - offset);
			if (generator() == realNumber) {
				seedDiscovered = true;
				break;
			}
		}
		
		if (!seedDiscovered) {
			throw std::runtime_error("Unable to discover generator's seed");
		}
		
		while (account.money < goal) {
			int32_t number = generator();
			MakeBetResponse response = makeBet(mt, account, account.money / 2, number);
			std::cout << response << std::endl;
			std::cout << "Current money: " << account.money << std::endl;
		}
	}
	
}
