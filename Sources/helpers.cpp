//
//  helpers.cpp
//  CasinoBreaker
//
//  Created by iKing on 16.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "helpers.hpp"

namespace helpers {
	
	EuclidResult extended_euclid(int64_t a, int64_t b) {
		if (b == 0) {
			return EuclidResult { a, 1, 0 };
		}
		EuclidResult result = extended_euclid(b, a % b);
		return EuclidResult { result.gcd, result.y, result.x - a / b * result.y };
	}
	
	
}
