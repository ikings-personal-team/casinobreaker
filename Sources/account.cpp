//
//  account.cpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "account.hpp"
#include <iostream>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <stdexcept>
#include "constants.hpp"

void to_json(json_t& json, const Account& a) {
	json = json_t{{"id", a.id}, {"money", a.money}, {"deletionTime", a.deletionTime}};
}

void from_json(const json_t& json, Account& a) {
	a.id = json.at("id").get<std::string>();
	a.money = json.at("money").get<int64_t>();
	a.deletionTime = json.at("deletionTime").get<std::string>();
}

Account Account::create(const std::string& id) {
	curlpp::Cleanup cleanup;
	
	std::ostringstream os;
	os << curlpp::options::Url(constants::AccountCreationUrl + "?id=" + id);
	
	json_t json = json_t::parse(os.str());
	
	if (json.find("error") != json.end()) {
		std::string error = json.at("error").get<std::string>();
		throw std::runtime_error(error);
	}
	
	return json;
}

std::ostream& operator<<(std::ostream& ostream, const Account& account) {
	ostream << "Account id: " << account.id << "; money: " << account.money << "; deletionDate: " << account.deletionTime;
	return ostream;
}
