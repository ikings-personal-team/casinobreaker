//
//  account.hpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#ifndef Account_hpp
#define Account_hpp

#include <stdio.h>
#include <string>
#include "json.hpp"

using json_t = nlohmann::json;

struct Account {
	
	std::string id;
	int64_t money;
	std::string deletionTime;
	
	static Account create(const std::string& id);
};

std::ostream& operator<<(std::ostream& ostream, const Account& account);

void to_json(json_t& json, const Account& a);
void from_json(const json_t& json, Account& a);

#endif /* Account_hpp */
