//
//  game-lcg.cpp
//  CasinoBreaker
//
//  Created by iKing on 16.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "game.hpp"
#include <vector>
#include <iostream>
#include "helpers.hpp"

namespace game {
	
	void playLcg(Account& account, int64_t goal) {
		using namespace helpers;
		
		int64_t a = 0;
		int64_t c = 0;
		int64_t m = pow(2.0, 32.0);
		int64_t lastNumber = 0;
		
		bool coefficientsCalculated = false;
		while (!coefficientsCalculated) {
			std::vector<int64_t> n(3);
			for (uint8_t i = 0; i < 3; ++i) {
				MakeBetResponse response = makeBet(lcg, account, 1, 0);
				n[i] = response.realNumber;
				lastNumber = response.realNumber;
				std::cout << response << std::endl;
				std::cout << "Current money: " << account.money << std::endl;
			}
			
			int64_t numerator = n[1] - n[2];
			int64_t denominator = n[0] - n[1];
			EuclidResult euclidResult = extended_euclid(denominator, m);
			if (euclidResult.gcd != 1) {
				continue;
			}
			
			int64_t denominatorReciprocal = euclidResult.x;
			a = numerator * denominatorReciprocal % m;
			if (a < 0) {
				a += m;
			}
			
			c = (n[2] - a * n[1]) % m;
			if (c < 0) {
				c += m;
			}
			
			coefficientsCalculated = true;
		}
		
		while (account.money < goal) {
			// Casting to int32_t simulates server's behavior
			int64_t number = (int32_t) (a * lastNumber + c) % m;
			MakeBetResponse response = makeBet(lcg, account, account.money / 2, number);
			lastNumber = response.realNumber;
			std::cout << response << std::endl;
			std::cout << "Current money: " << account.money << std::endl;
		}
	}
	
}
