//
//  constants.hpp
//  CasinoBreaker
//
//  Created by iKing on 15.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#ifndef constants_h
#define constants_h

#include <string>

namespace constants {
	
	const std::string ServerUrl = "http://ec2-35-159-11-170.eu-central-1.compute.amazonaws.com/casino";
	const std::string AccountCreationUrl = ServerUrl + "/createacc";
	const std::string PlayUrl = ServerUrl + "/play";
}

#endif /* constants_h */
