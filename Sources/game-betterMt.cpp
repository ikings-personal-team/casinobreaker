//
//  game-betterMt.cpp
//  CasinoBreaker
//
//  Created by iKing on 18.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include "game.hpp"
#include <vector>
#include <iostream>
#include <random>

namespace game {
	
	u_int32_t reservseBitshiftRightXor(u_int32_t value, u_int32_t shift, u_int32_t mask = std::numeric_limits<u_int32_t>::max()) {
		u_int32_t offset = 0;
		u_int32_t result = 0;
		while (offset * shift < 32) {
			u_int32_t partMask = ((u_int32_t) -1 << (32 - shift)) >> (shift * offset);
			u_int32_t part = value & partMask;
			value ^= (part >> shift) & mask;
			result |= part;
			offset++;
		}
		return result;
	}
	
	u_int32_t reservseBitshiftLeftXor(u_int32_t value, u_int32_t shift, u_int32_t mask = std::numeric_limits<u_int32_t>::max()) {
		u_int32_t offset = 0;
		u_int32_t result = 0;
		while (offset * shift < 32) {
			u_int32_t partMask = ((u_int32_t) -1 >> (32 - shift)) << (shift * offset);
			u_int32_t part = value & partMask;
			value ^= (part << shift) & mask;
			result |= part;
			offset++;
		}
		return result;
	}
	
	void playBetterMt(Account& account, int64_t goal) {
		
		std::vector<u_int32_t> state(624);
		for (size_t i = 0; i < 624; ++i) {
			MakeBetResponse response = makeBet(betterMt, account, 1, 0);
			u_int32_t number = (u_int32_t) response.realNumber;
			number = reservseBitshiftRightXor(number, 18);
			number = reservseBitshiftLeftXor(number, 15, 0xEFC60000);
			number = reservseBitshiftLeftXor(number, 7, 0x9D2C5680);
			number = reservseBitshiftRightXor(number, 11);
			state[i] = number;
		}
		
		std::stringstream ioss;
		std::copy(state.begin(), state.end(), std::ostream_iterator<u_int32_t>(ioss, " "));
		
		std::mt19937 generator;
		ioss >> generator;
		
		while (account.money < goal) {
			int32_t number = generator();
			MakeBetResponse response = makeBet(betterMt, account, account.money / 2, number);
			std::cout << response << std::endl;
			std::cout << "Current money: " << account.money << std::endl;
		}
	}
	
}
